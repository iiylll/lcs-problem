# README #

This README would normally document whatever steps are necessary to get application up and running.

### Used versions

* Java 1.8+
* Node.js 14.5.0
* NPM 6.14.5

### How do I get set up? ###

####Start Backend
```
cd backend
mvn spring-boot:run
```
if not working, please use wrapper
`./mvnw spring-boot:run`


####Start Frontend

```
cd frontend
npm install
yarn start
```

Run application with web browser: http://localhost:3000/
