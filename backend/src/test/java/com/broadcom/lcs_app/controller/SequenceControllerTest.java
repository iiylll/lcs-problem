package com.broadcom.lcs_app.controller;

import com.broadcom.lcs_app.dao.SequenceRepository;
import com.broadcom.lcs_app.model.Sequence;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.util.Arrays;

@ContextConfiguration(classes = {SequenceController.class})
@AutoConfigureMockMvc
@WebMvcTest
public class SequenceControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SequenceRepository sequenceRepository;

    @BeforeEach
    void setUp(){
        final Sequence sequence = new Sequence("asd");
        final Sequence secondSequence = new Sequence("asd");

        given(sequenceRepository.findAll()).willReturn(Arrays.asList(sequence, secondSequence));
    }

    @Test
    void test() throws Exception{
        mvc.perform(get("/sequence"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andDo(MockMvcResultHandlers.print());
    }

}
