package com.broadcom.lcs_app.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class LongestCommonSubsequenceServiceTest {
    @Autowired
    LongestCommonSubsequenceService lcsService;

    @Test
    void threePairTest(){
        assertEquals("837", lcsService.calculate(
                Arrays.asList("832174", "6813147", "8327")
        ));

        assertEquals("ABD", lcsService.calculate(
                Arrays.asList("ABCD", "ACBAD", "ABD")
        ));

        assertEquals("AB", lcsService.calculate(
                Arrays.asList("ABCD", "ACBAD", "AB")
        ));

        assertEquals("", lcsService.calculate(
                Arrays.asList("ABCD", "ACBAD", "E")
        ));
    }

    @Test
    void onePairTestPositiveFirst(){
        Set<String> expectedLcs = Stream.of("ABD", "ACD")
                .collect(Collectors.toCollection(HashSet::new));
        List<String> multipleLcs = lcsService.lcs("ACBAD", "ABCD");
        assertEquals(2, multipleLcs.size());
        assertTrue(multipleLcs.stream().allMatch(x -> expectedLcs.contains(x)));
    }

    @Test
    void onePairTestPositiveSecond(){
        Set<String> expectedLcs = Stream.of("8317", "8314")
                .collect(Collectors.toCollection(HashSet::new));
        List<String> multipleLcs = lcsService.lcs("6813147", "832174");
        assertEquals(2, multipleLcs.size());
        assertTrue(multipleLcs.stream().allMatch(x -> expectedLcs.contains(x)));
    }

    @Test
    void emptyStringTests(){
        List<String> answer = lcsService.lcs("ABADE", "");
        assertFalse(answer.isEmpty());
        assertTrue(answer.get(0).isEmpty());
        answer = lcsService.lcs("", "");
        assertFalse(answer.isEmpty());
        assertTrue(answer.get(0).isEmpty());

    }
}
