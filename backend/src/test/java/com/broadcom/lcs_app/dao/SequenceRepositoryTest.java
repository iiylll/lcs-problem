package com.broadcom.lcs_app.dao;

import com.broadcom.lcs_app.model.Sequence;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class SequenceRepositoryTest {
    @Autowired
    private SequenceRepository sequenceRepository;

    @Test
    void insertSmokeTest() {
        final Sequence sequence = Sequence.builder().build();
        assertEquals(0, sequenceRepository.count());
        sequenceRepository.save(sequence);
        assertEquals(1, sequenceRepository.count());
    }

    @Test
    void insertSequenceIncludeWordsTest() {
        final Sequence sequence = new Sequence("asd");
        final Sequence secondSequence = new Sequence("asd");

        sequenceRepository.saveAll(Arrays.asList(sequence, secondSequence));
        assertEquals(2, sequenceRepository
                .findAll().stream().map(Sequence::getText).count());
    }
}
