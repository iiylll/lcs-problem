package com.broadcom.lcs_app.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="sequence")
public class Sequence{
    @Id
    @GeneratedValue
    @JsonProperty
    private long id;

    @JsonProperty
    private String text;

    public Sequence(String text){
        this.text = text;
    }
}
