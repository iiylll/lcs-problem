package com.broadcom.lcs_app.service;

import com.broadcom.lcs_app.dao.SequenceRepository;
import com.broadcom.lcs_app.dto.LCSResponse;
import com.broadcom.lcs_app.model.Sequence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Integer.max;

@Service
public class LongestCommonSubsequenceService {
    @Autowired
    private SequenceRepository sequenceRepository;

    public LCSResponse calculate(){
        return new LCSResponse(calculate(sequenceRepository.findAll()
                .stream()
                .map(Sequence::getText)
                .collect(Collectors.toList())));
    }

    //Needleman-Wunsch algo
    public String calculate(List<String> texts){
        List<String> answer = new ArrayList<>();
        for(int i = 0; i < texts.size()-1; i++){
            if(answer.isEmpty()){
                answer = lcs(texts.get(i), texts.get(i+1));
            } else {
                int finalI = i;
                answer = answer.stream()
                        .map(x -> lcs(x, texts.get(finalI +1)))
                        .filter(x -> !x.get(0).isEmpty())
                        .flatMap(List::stream)
                        .collect(Collectors.toList());
            }
        }
        return answer.stream().max(Comparator.comparingInt(String::length)).orElse("");
    }

    private int[][] createMatrix(String x, String y){
        int[][] matrix = new int[x.length()+1][y.length()+1];
        for(int i = 0; i < matrix.length - 1 ; i++){
            for (int j = 0; j < matrix[i].length - 1; j++){
                if(x.charAt(i) == y.charAt(j)){
                    matrix[i+1][j+1] = matrix[i][j] + 1;
                } else {
                    matrix[i+1][j+1] = max(matrix[i+1][j], matrix[i][j+1]);
                }
            }
        }
        return matrix;
    }

    public List<String> lcs(String x, String y){
        return new ArrayList<>((new HashSet<>(lcs(createMatrix(x, y), x, y, x.length(), y.length()))));
    }

    public List<String> lcs(int[][] matrix, String x, String y, int i, int j){
        if (i == 0 || j == 0){
            return Collections.singletonList("");
        }
        if (matrix[i-1][j] == matrix[i][j] && matrix[i][j] == matrix[i][j-1]){
            List<String> up = new ArrayList<>(lcs(matrix, x, y, i-1, j));
            List<String> left = new ArrayList<>(lcs(matrix, x, y, i, j - 1));
            up.addAll(left);
            return up;
        } else if (matrix[i-1][j] == matrix[i][j]){
            return lcs(matrix, x, y, i-1, j);
        } else if (matrix[i][j-1] == matrix[i][j]){
            return lcs(matrix, x, y, i, j-1);
        } else if (matrix[i-1][j-1] < matrix[i][j]){
            String fixedChar = String.valueOf(x.charAt(i-1));
            List<String> previousString = lcs(matrix, x, y, i-1, j-1);
            return previousString.stream().map(str -> str + fixedChar).collect(Collectors.toList());
        } else {
           throw new IllegalStateException();
        }
    }


}
