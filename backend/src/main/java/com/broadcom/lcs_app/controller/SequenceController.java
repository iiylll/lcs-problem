package com.broadcom.lcs_app.controller;

import com.broadcom.lcs_app.dao.SequenceRepository;
import com.broadcom.lcs_app.model.Sequence;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/sequence")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:3000" })
public class SequenceController {
    private final SequenceRepository sequenceRepository;

    @GetMapping
    public List<Sequence> findAll(){
        return sequenceRepository.findAll();
    }

    @PostMapping
    public Sequence create(@RequestBody Sequence sequence){
        return sequenceRepository.save(sequence);
    }

    @DeleteMapping
    public void deleteAll(){
        sequenceRepository.deleteAll();;
    }

}
