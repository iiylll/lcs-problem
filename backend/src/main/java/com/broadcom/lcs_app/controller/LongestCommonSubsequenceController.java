package com.broadcom.lcs_app.controller;

import com.broadcom.lcs_app.dto.LCSResponse;
import com.broadcom.lcs_app.service.LongestCommonSubsequenceService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/lcs")
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:3000" })
public class LongestCommonSubsequenceController {

    LongestCommonSubsequenceService lcsService;

    @PostMapping
    public LCSResponse create(){
        return lcsService.calculate();
    }
}
