package com.broadcom.lcs_app.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class LCSResponse {
    @JsonProperty
    private String subsequence;
}
