package com.broadcom.lcs_app.dao;

import com.broadcom.lcs_app.model.Sequence;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SequenceRepository extends CrudRepository<Sequence, Long> {
    @Override
    List<Sequence> findAll();
}
