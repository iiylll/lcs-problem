import logo from './logo.svg';
import './App.css';
import React, {Component} from "react";
import Button from 'react-bootstrap/Button';
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Form from "react-bootstrap/Form";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import Alert from "react-bootstrap/Alert";

const BASE_URL = "http://localhost:8080"

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sequences: [],
            lcs: null
        }
    }

    componentDidMount() {
        this.fetchSequences();
    }

    fetchSequences() {
        let self = this;
        fetch(BASE_URL + '/sequence', {
            method: 'GET'
        }).then(function (response) {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(function (data) {
            console.log(data);
            self.setState({sequences: data});
        }).catch(err => {
            console.log('caught it!', err);
        })
    }

    parseLcs(result) {
        let self = this;
        result.then(function (data) {
            console.log(data['subsequence']);
            self.setState({lcs: data['subsequence']})
            console.log(self.state);
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <div style={{"height": "100px"}}></div>
                </Row>
                <Row>
                    <Col xs={6}>
                        <SequenceForm onSend={() => this.fetchSequences()}/>
                        <div style={{"height": "30px"}}></div>
                        <LcsResult lcs={this.state.lcs}/>
                    </Col>
                    <Col xs={6}>
                        <SequenceList sequences={this.state.sequences}/>
                        <Row>
                            <Col>
                                <LCSComponent onResult={(result) => this.parseLcs(result)}/>
                            </Col>
                            <Col>
                                <DeleteComponent/>
                            </Col>
                        </Row>
                    </Col>

                </Row>
            </Container>
        );
    }

}

class LcsResult extends Component {
    componentWillReceiveProps(props) {
        this.setState({lcs: props.lcs});
    }

    constructor(props) {
        super(props);
        this.state = {
            lcs: this.props.lcs
        }
    }

    render() {
        let lcsResult;
        if (this.state.lcs != null) {
            lcsResult = <Card style={{width: '18rem'}}>
                <Card.Body>
                    <Card.Title>LCS</Card.Title>
                    <Card.Text>
                        Last counted value is: "{this.state.lcs}"
                    </Card.Text>
                </Card.Body>
            </Card>
        } else {
            lcsResult = <Row/>
        }
        return lcsResult;
    }
}

class SequenceList extends Component {
    componentWillReceiveProps(props) {
        this.setState({sequences: props.sequences});
    }

    constructor(props) {
        super(props);
        this.state = {
            sequences: this.props.sequences
        }
        console.log(this.props.sequences);
    }

    render() {
        let sequenceComponent;
        if (this.state.sequences.length > 0) {
            sequenceComponent = this.state.sequences.map(seq =>
                <Card key={seq.id}>
                    <Card.Body>{seq.text}</Card.Body>
                </Card>
            )
        } else {
            sequenceComponent = <Alert variant='primary'>
                No sequences has been created, please create some!
            </Alert>
        }
        return sequenceComponent;
    }
}

class LCSComponent extends Component {

    handleSubmit = (event) => {
        let self = this;
        fetch(BASE_URL + '/lcs', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain',
                'Content-Type': 'application/json;charset=UTF-8'
            },
        }).then(function (response) {
            console.log(response)
            self.props.onResult(response.json());
        });
        event.preventDefault();
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Button variant="success" type="submit">
                    Calculate LCS
                </Button>
            </Form>
        );
    }
}

class DeleteComponent extends Component {
    handleSubmit = (event) => {
        fetch(BASE_URL + '/sequence', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json, text/plain',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(this.state)
        }).then(function (response) {
            console.log(response)
            return response.json();
        });
        this.props.onSend();
        event.preventDefault();
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Button variant="danger" type="submit">
                    Delete all
                </Button>
            </Form>
        );
    }
}

class SequenceForm extends Component {
    constructor(props) {
        super(props);
        this.state = {text: ''};
    }

    handleChange = (event) => {
        console.log(event.target.value);
        this.setState({[event.target.name]: event.target.value});
    }

    handleSubmit = (event) => {
        let self = this;
        fetch(BASE_URL + '/sequence', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain',
                'Content-Type': 'application/json;charset=UTF-8'
            },
            body: JSON.stringify(this.state)
        }).then(function (response) {
            console.log(response)
            self.props.onSend();
            return response.json();
        });
        event.preventDefault();
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="formSequence">
                    <Form.Control type="text" placeholder="List of words separated by white space" name="text"
                                  value={this.state.value} onChange={this.handleChange}/>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Create new
                </Button>
            </Form>
        );
    }
}

export default App;
